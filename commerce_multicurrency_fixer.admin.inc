<?php
/**
 * @file
 * Administrative UI for commerce multicurrency fixer.
 */

/**
 * Payment method callback: settings form.
 */
function commerce_multicurrency_fixer_settings_form($form, &$form_state) {

  $enabled_currencies = commerce_currencies(TRUE);
  $default_currency_code = commerce_default_currency();
  $conversion_settings = variable_get('commerce_multicurrency_conversion_settings', array());
  $conversion_settings = variable_get('commerce_multicurrency_conversion_settings', array());
  $use_cross_sync = variable_get('commerce_multicurrency_use_cross_conversion', TRUE);

  $form = array();

  $form['commerce_multicurrency_fixer_access_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Your API Access Key'),
    '#description' => t('A unique key assigned to each API account used to authenticate with the API.'),
    '#default_value' => variable_get('commerce_multicurrency_fixer_access_key', ''),
    '#size' => 32,
    '#required' => TRUE,
  );
  $form['commerce_multicurrency_fixer_base'] = array(
    '#type' => 'select',
    '#title' => t('Base currency'),
    '#description' => t('The currency to which exchange rates are relative to. (If 1 USD = X EUR, USD is the base currency)'),
    '#options' => $enabled_currencies,
    '#default_value' => variable_get('commerce_multicurrency_fixer_base', NULL),
  );
  
  $form['commerce_multicurrency_fixer_currencies'] = array(
    '#type' => 'select',
    '#title' => t('Limit currencies'),
    '#description' => t('To reduce bandwidth you can limit the number of currencies synced from the Fixer API.'),
    '#options' => $enabled_currencies,
    '#default_value' => variable_get('commerce_multicurrency_fixer_currencies', NULL),
  );

  return $form;
}
