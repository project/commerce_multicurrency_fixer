Fixer exchange rates provider for Commerce multicurrency module.

Foreign exchange rates and currency conversion JSON API

DEPENDENCIES
Commerce Multicurrency Fixer depends on the Commerce multicurrency module.

INSTALLATION
Install the module as usual.

CONFIGURATION
1. Goto https://fixer.io/ and signup for a free basic plan to 
   get API Access credentials
2. On your our site goto the "Fixer" settings page and input API credentials at:
   admin/commerce/config/currency/conversion/fixer
3. Next goto Select "Fixer" on the currency conversion settings page at:
   admin/commerce/config/currency/conversion
4. Run cron or sync manually to synchronize the rates.
