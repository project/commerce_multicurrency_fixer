<?php

/**
 * @file
 * Default currency sync callback
 */

/**
 * Fetch the currency exchange rates for the requested currency combination.
 *
 * Return an array with the array(target_currency_code => rate) combination.
 *
 * @param string $currency_code
 *   Source currency code.
 * @param array $target_currencies
 *   Array with the target currency codes.
 *
 * @return array
 *   Array with the array(target_currency_code => rate) combination.
 */
function commerce_multicurrency_fixer_exchange_rate_sync_provider_fixer($currency_code, $target_currencies) {
  $data = cache_get(__FUNCTION__, 'cache');
  $currency_base = variable_get('commerce_multicurrency_fixer_base', NULL);

  if (!$data) {
    $decoded_rate = null;
    $fixer_rates = array();
    $current_currency = commerce_multicurrency_get_user_currency_code();
    $enabled_currencies = commerce_currencies(TRUE);
    $default_currency_code = commerce_default_currency();
    //  http://data.fixer.io/api/latest
    //  ?access_key=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    //  &base=GBP
    //  &symbols=EUR,USD,AUD,CAD

    $api_url = 'http://data.fixer.io/api/latest';
    $api_key = variable_get('commerce_multicurrency_fixer_access_key', NULL);
    $api_url_part = 'access_key=' . $api_key;
    $currency_url_part = '&base=' . $currency_base;
    // @todo add filtering for fewer symbols to 
//    $currency_requested = '&symbols=' . variable_get('commerce_multicurrency_fixer_currencies', NULL);
    $currency_requested = '';

    $endpoint = $api_url . '?' . $api_url_part . $currency_url_part . $currency_requested;
    if ($rate = file_get_contents($endpoint)) {
      $decoded_rate = json_decode($rate, true);
    }

    if (($rate) && @count($decoded_rate['rates'])) {
      foreach ($decoded_rate['rates'] as $key => $value) {
        $fixer_rates[(string) $key] = (string) $value;
      }
      // Cache six hours.
      cache_set(__FUNCTION__, $fixer_rates, 'cache', time() + (3600 * 6));
    }
    else {
      watchdog(
          'commerce_multicurrency', 'Rate provider Fixer: Unable to fetch / process the currency data of @url', array('@url' => $api_url), WATCHDOG_ERROR
      );
    }
  }
  else {
    $fixer_rates = $data->data;
  }

  $rates = array();
  foreach ($target_currencies as $target_currency_code) {
    if ($currency_code == $currency_base && isset($fixer_rates[$target_currency_code])) {
      $rates[$target_currency_code] = $fixer_rates[$target_currency_code];
    }
    elseif (isset($fixer_rates[$currency_code]) && $target_currency_code == $currency_base) {
      // Reverse rate calculation.
      $rates[$target_currency_code] = 1 / $fixer_rates[$currency_code];
    }
    elseif (isset($fixer_rates[$currency_code]) && isset($fixer_rates[$target_currency_code])) {
      // Cross rate calculation.
      $rates[$target_currency_code] = $fixer_rates[$target_currency_code] / $fixer_rates[$currency_code];
    }
  }
  return $rates;
}
